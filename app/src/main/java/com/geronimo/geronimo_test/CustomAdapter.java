package com.geronimo.geronimo_test;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;


public class CustomAdapter extends BaseAdapter {

    private List<Row> rows = new ArrayList<>();

    private Context context;

    public CustomAdapter(Context context) {
        this.context = context;
    }

    public List<Row> getList() {
        return rows;
    }

    public void add(Row row) {
        this.rows.add(row);
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return rows.size();
    }

    @Override
    public Object getItem(int position) {
        return rows.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();
        LayoutInflater messageInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        Row row = rows.get(position);
        convertView = messageInflater.inflate(R.layout.custom_list, null);
        holder.description = convertView.findViewById(R.id.description_list);
        holder.description.setText(row.getDescription());
        holder.title = convertView.findViewById(R.id.title_list);
        holder.title.setText(row.getTitle());
        holder.image = convertView.findViewById(R.id.image_list);
        holder.image.setImageBitmap(row.getBitmap());
        convertView.setTag(holder);
        return convertView;
    }

    class ViewHolder {
        public TextView description;
        public TextView title;
        public ImageView image;
    }
}
