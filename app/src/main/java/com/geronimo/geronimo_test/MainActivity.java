package com.geronimo.geronimo_test;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    CustomAdapter customAdapter;
    Button detailButton;
    Button shareButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.listview);
        customAdapter = new CustomAdapter(this);

        detailButton = findViewById(R.id.detail_button);
        shareButton = findViewById(R.id.share_button);

        customAdapter.add(new Row(BitmapFactory.decodeResource(getResources(),R.drawable.background),"The King Background 1","Background"));
        customAdapter.add(new Row(BitmapFactory.decodeResource(getResources(),R.drawable.background),"The King Background 2","Background"));
        customAdapter.add(new Row(BitmapFactory.decodeResource(getResources(),R.drawable.background),"The King Background 3","Background"));
        customAdapter.add(new Row(BitmapFactory.decodeResource(getResources(),R.drawable.background),"The King Background 4","Background"));
        customAdapter.add(new Row(BitmapFactory.decodeResource(getResources(),R.drawable.background),"The King Background 5","Background"));

        listView.setAdapter(customAdapter);

    }


    public void detail(View view) {
        Button button =  (Button) view;

        button.performClick();

        button.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.performClick();
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        v.setBackgroundColor(Color.RED);
                        break;
                    case MotionEvent.ACTION_UP:
                        v.performClick();
                        v.setBackgroundColor(Color.TRANSPARENT);
                        TextView detail = findViewById(R.id.description_list);
                        Toast.makeText(getApplicationContext(),detail.getText().toString(),Toast.LENGTH_SHORT).show();
                        break;
                }
                return false;
            }
        });

    }

    public void share(View view) {

        TextView detailTextView = findViewById(R.id.description_list);

        String detail = detailTextView.getText().toString();

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT,detail);
        startActivity(Intent.createChooser(intent,"Complete action using"));
    }
}
