package com.geronimo.geronimo_test;

import android.graphics.Bitmap;

public class Row {

    Bitmap bitmap;
    String description;
    String title;

    public Row(Bitmap bitmap, String description, String title) {
        this.bitmap = bitmap;
        this.description = description;
        this.title = title;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }
}
